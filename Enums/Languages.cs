﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Enums
{
    public enum Languages
    {
        Arabic = 1,
        English = 2,
    }
}