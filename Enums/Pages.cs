﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Enums
{
    public enum Pages
    {
        ProductCategories = 1,
        Suppliers = 2,
        TendarStages = 3,
        Products = 4,
        Proposals = 5,
        Tasks = 6,
        Accounts = 7,
        Tendars = 8,
        Users = 9,
        OfferTypes = 10,
        Reports = 11
    }
}