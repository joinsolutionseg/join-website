﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Enums
{
    public enum ProposalTypes
    {
        Join = 1,
        Assisted = 2
    }
}