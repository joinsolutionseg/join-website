﻿function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

function setCookie(ck_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    //alert(value)
    var ck_value = value + ((exdays == null) ? "" : "; path=/;expires=" + exdate.toUTCString());
    //alert(ck_value)
    document.cookie = ck_name + "=" + ck_value;
};

//evwnt to change language in web
$("#fold").click(function () {
    //debugger;
    var enLang = $("#ENLang").val();
    var arLang = $("#ARLang").val();
    //alert(enLang)
    //alert(arLang)
    var url = window.location.href;
   
    $("#fold_p").fadeOut(function () {
        if ($("#fold_p").text() == enLang) {
            $("#fold_p").text(enLang).fadeIn();
            setCookie("JoinLang", 'en', 30);
            url = url.replace("/ar/", "/en/");
        }
        else {
            $("#fold_p").text(arLang).fadeIn();
            setCookie("JoinLang", 'ar', 30);
            url = url.replace("/en/", "/ar/");
        }
        window.location = url;
    });
});
