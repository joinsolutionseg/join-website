﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class ProductsController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: Products
        public ActionResult Index()
        {
            var products = db.Products.OrderByDescending(o => o.id).Include(p => p.ProductCategory).Include(p => p.Supplier);
            return View(products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.productCategoryId = new SelectList(db.ProductCategories, "id", "name");
            ViewBag.supplierId = new SelectList(db.Suppliers, "id", "name");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,productSKU,productNameEn,productNameAr,productCategoryId,supplierId,productDescriptionEn,productDescriptionAr")] ProductModel product)
        {
            if (ModelState.IsValid)
            {
                var productEntity = new Product()
                {
                    id = product.id,
                    productCategoryId = product.productCategoryId,
                    productDescriptionAr = product.productDescriptionAr,
                    productDescriptionEn = product.productDescriptionEn,
                    productNameAr = product.productNameAr,
                    productNameEn = product.productNameEn,
                    productSKU = product.productSKU,
                    supplierId = product.supplierId
                };
                db.Products.Add(productEntity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.productCategoryId = new SelectList(db.ProductCategories, "id", "name", product.productCategoryId);
            ViewBag.supplierId = new SelectList(db.Suppliers, "id", "name", product.supplierId);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.productCategoryId = new SelectList(db.ProductCategories, "id", "name", product.productCategoryId);
            ViewBag.supplierId = new SelectList(db.Suppliers, "id", "name", product.supplierId);

            var productModel = new ProductModel()
            {
                productCategoryId = product.productCategoryId,
                productDescriptionAr = product.productDescriptionAr,
                productDescriptionEn = product.productDescriptionEn,
                productNameAr = product.productNameAr,
                productNameEn = product.productNameEn,
                productSKU = product.productSKU,
                id = product.id,
                supplierId = product.supplierId,
            };

            return View(productModel);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,productSKU,productNameEn,productNameAr,productCategoryId,supplierId,productDescriptionEn,productDescriptionAr")] ProductModel product)
        {
            if (ModelState.IsValid)
            {
                var productEntity = new Product()
                {
                    id = product.id,
                    productCategoryId = product.productCategoryId,
                    productDescriptionAr = product.productDescriptionAr,
                    productDescriptionEn = product.productDescriptionEn,
                    productNameAr = product.productNameAr,
                    productNameEn = product.productNameEn,
                    productSKU = product.productSKU,
                    supplierId = product.supplierId
                };
                db.Entry(productEntity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.productCategoryId = new SelectList(db.ProductCategories, "id", "name", product.productCategoryId);
            ViewBag.supplierId = new SelectList(db.Suppliers, "id", "name", product.supplierId);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
