﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Enums;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Helpers;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class EstimatesController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: Estimates

        public ActionResult InvoiceNew()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View(db.Estimates.OrderByDescending(o => o.id).ToList());
        }

        // GET: Estimates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estimate estimate = db.Estimates.Find(id);
            if (estimate == null)
            {
                return HttpNotFound();
            }
            return View(estimate);
        }

        // GET: Estimates/Create
        public ActionResult Create(int? tenderId)
        {
            var ss = CultureHelper.CurrentLanguage;
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName");
            ViewBag.offerTypeId = new SelectList(db.OfferTypes, "offerTypeId", CultureHelper.CurrentLanguage == (int)Languages.English ? "offerNameEn" : "offerNameAr");
            return View(new EstimatesModel() { estimateDate = LocalDateTime.GetDateTimeNow(), currencyRate = (decimal)3.79 });
        }

        // POST: Estimates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EstimatesModel estimate)
        {
            if (ModelState.IsValid)
            {
                var estimateEntity = new Estimate()
                {
                    currencyRate = estimate.currencyRate,
                    customerName = "",
                    customerNotes = estimate.customerNotes,
                    description = estimate.description,
                    estimateDate = estimate.estimateDate,
                    estimateTerms = estimate.estimateTerms,
                    statusId = 1,
                    customerEmail = estimate.customerEmail,
                    customerPhone = estimate.customerPhone,
                    createdById = ((User)Session["CurrentUser"]).id,
                    tenderId = estimate.tenderId,
                    accountId = estimate.accountId,
                    offerTypeId = estimate.offerTypeId
                };
                db.Estimates.Add(estimateEntity);
                db.SaveChanges();
                return RedirectToAction("Edit", new { estimateEntity.id });
            }

            return View(estimate);
        }

        // GET: Estimates/Edit/5
        public ActionResult Edit(int? id)
        {

            ViewBag.productId = new SelectList(db.Products, "id", "productSKU", "productId");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estimate estimate = db.Estimates.Find(id);
            if (estimate == null)
            {
                return HttpNotFound();
            }
            var estimateModel = new EstimatesModel()
            {
                id = estimate.id,
                currencyRate = estimate.currencyRate,
                customerName = "",
                customerNotes = estimate.customerNotes,
                description = estimate.description,
                estimateDate = estimate.estimateDate,
                estimateTerms = estimate.estimateTerms,
                statusId = 1,
                customerEmail = estimate.customerEmail,
                customerPhone = estimate.customerPhone,
                tenderId = estimate.tenderId,
                accountId = estimate.accountId,
                offerTypeId = estimate.offerTypeId
            };

            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", estimateModel.accountId);
            ViewBag.offerTypeId = new SelectList(db.OfferTypes, "offerTypeId", CultureHelper.CurrentLanguage == (int)Languages.English ? "offerNameEn" : "offerNameAr", estimateModel.offerTypeId);

            estimateModel.EstimateProducts = db.EstimateProducts.Where(o => o.estimateId == id).ToList();

            return View(estimateModel);
        }

        // POST: Estimates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EstimatesModel estimate)
        {
            if (ModelState.IsValid)
            {
                var estimateEntity = new Estimate()
                {
                    currencyRate = estimate.currencyRate,
                    customerName = "",
                    customerNotes = estimate.customerNotes,
                    description = estimate.description,
                    estimateDate = estimate.estimateDate,
                    estimateTerms = estimate.estimateTerms,
                    statusId = 1,
                    id = estimate.id,
                    customerEmail = estimate.customerEmail,
                    customerPhone = estimate.customerPhone,
                    createdById = ((User)Session["CurrentUser"]).id,
                    tenderId = estimate.tenderId,
                    accountId = estimate.accountId,
                    offerTypeId = estimate.offerTypeId
                };

                db.Entry(estimateEntity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { estimateEntity.id });
            }
            return View(estimate);
        }

        // GET: Estimates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estimate estimate = db.Estimates.Find(id);
            if (estimate == null)
            {
                return HttpNotFound();
            }
            return View(estimate);
        }

        // POST: Estimates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Estimate estimate = db.Estimates.Find(id);

            if (estimate.EstimateProducts.Count() > 0)
            {
                var list = db.EstimateProducts.Where(o => o.estimateId == id);
                foreach (var esPro in list)
                {
                    db.EstimateProducts.Remove(esPro);
                }
            }

            db.Estimates.Remove(estimate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Invoice(int estimateId)
        {
            var invoices = db.Estimates.FirstOrDefault(e => e.id == estimateId);
            if (invoices.offerTypeId == (int)ProposalTypes.Assisted)
                return View("InvoiceNew", invoices);

            return View(invoices);
        }
    }
}
