﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Enums;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Helpers;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class CrmAccountTasksController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: CrmAccountTasks
        public ActionResult Index(int accountId)
        {
            var accountTasks = db.AccountTasks.Where(m => m.accountId == accountId)
                .Include(a => a.Account).Include(a => a.Task).Include(a => a.User).OrderByDescending(m=> m.isCompleted == false).ToList();
            ViewBag.assignedToId = db.Users.Where(o=>o.UserPagePermissions.Any(oo=>oo.pageId == (int)Pages.Tasks)).Select(x => new SelectListItem() { Text = x.username, Value = x.id.ToString() });

            return View(accountTasks.ToList());
        }

        public JsonResult EditAccountTask(int id, bool? isCompleted,int? assignedTo, string comment)
        {
            AccountTask accountTask = db.AccountTasks.Find(id);

            accountTask.isCompleted = (bool)isCompleted;
            accountTask.Comment = comment;
            accountTask.assignedToId = assignedTo;
            accountTask.updatedDate = LocalDateTime.GetDateTimeNow();
            accountTask.updatedById = ((User)Session["CurrentUser"]).id;
            db.SaveChanges();
            return Json(true);
        }

        // GET: CrmAccountTasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountTask accountTask = db.AccountTasks.Find(id);
            if (accountTask == null)
            {
                return HttpNotFound();
            }
            return View(accountTask);
        }

        // GET: CrmAccountTasks/Create
        public ActionResult Create()
        {
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName");
            ViewBag.taskId = new SelectList(db.Tasks, "taskId", "taskName");
            ViewBag.createdById = new SelectList(db.Users, "id", "username");
            return View();
        }

        // POST: CrmAccountTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "accountTaskId,accountId,taskId,isCompleted,Comment,createdById,assignedToId,createdDate")] AccountTask accountTask)
        {
            if (ModelState.IsValid)
            {
                accountTask.createdDate = LocalDateTime.GetDateTimeNow();
                db.AccountTasks.Add(accountTask);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", accountTask.accountId);
            ViewBag.taskId = new SelectList(db.Tasks, "taskId", "taskName", accountTask.taskId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", accountTask.createdById);
            return View(accountTask);
        }

        // GET: CrmAccountTasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountTask accountTask = db.AccountTasks.Find(id);
            if (accountTask == null)
            {
                return HttpNotFound();
            }
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", accountTask.accountId);
            ViewBag.taskId = new SelectList(db.Tasks, "taskId", "taskName", accountTask.taskId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", accountTask.createdById);
            return View(accountTask);
        }

        // POST: CrmAccountTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "accountTaskId,accountId,taskId,isCompleted,Comment,createdById,assignedToId,createdDate")] AccountTask accountTask)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accountTask).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", accountTask.accountId);
            ViewBag.taskId = new SelectList(db.Tasks, "taskId", "taskName", accountTask.taskId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", accountTask.createdById);
            return View(accountTask);
        }

        // GET: CrmAccountTasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountTask accountTask = db.AccountTasks.Find(id);
            if (accountTask == null)
            {
                return HttpNotFound();
            }
            return View(accountTask);
        }

        // POST: CrmAccountTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AccountTask accountTask = db.AccountTasks.Find(id);
            db.AccountTasks.Remove(accountTask);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
