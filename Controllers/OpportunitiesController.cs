﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Enums;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Helpers;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class OpportunitiesController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: Opportunities
        public ActionResult Index()
        {
            var opportunities = db.Opportunities.Include(o => o.OpportunityStage).Include(o => o.User).Include(o => o.User1);
            return View(opportunities.ToList());
        }

        // GET: Opportunities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opportunity opportunity = db.Opportunities.Find(id);
            if (opportunity == null)
            {
                return HttpNotFound();
            }
            return View(opportunity);
        }

        // GET: Opportunities/Create
        public ActionResult Create(int? accountId)
        {
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", accountId);
            ViewBag.opportunityStageId = new SelectList(db.OpportunityStages, "opportunityStageId", "opportunityStageName");
            ViewBag.assignedToId = new SelectList(db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)), "id", "username");
            ViewBag.createdById = new SelectList(db.Users, "id", "username");
            return View();
        }

        // POST: Opportunities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OpportunityModel opportunity)
        {
            if (ModelState.IsValid)
            {
                var opportunityEntity = new Opportunity()
                {
                    opportunityStageId = opportunity.opportunityStageId,
                    assignedToId = opportunity.assignedToId,
                    opportunityDescription = opportunity.opportunityDescription,
                    opportunityEnded = opportunity.opportunityEnded,
                    opportunityId = opportunity.opportunityId,
                    opportunityName = opportunity.opportunityName,
                    tenderPapersPrice = opportunity.tenderPapersPrice,
                    tenderCost = opportunity.tenderCost,
                    accountId = Convert.ToInt32(Session["accountId"]) == 0 ? (int?)null : Convert.ToInt32(Session["accountId"])
                };
                opportunityEntity.createdDate = LocalDateTime.GetDateTimeNow();
                opportunityEntity.createdById = ((User)Session["CurrentUser"]).id;
                db.Opportunities.Add(opportunityEntity);
                db.SaveChanges();
                return RedirectToAction("Create", "Estimates",new { tenderId = opportunityEntity.opportunityId});
            }

            ViewBag.opportunityStageId = new SelectList(db.OpportunityStages, "opportunityStageId", "opportunityStageName", opportunity.opportunityStageId);
            ViewBag.assignedToId = new SelectList(db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)), "id", "username", opportunity.assignedToId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", opportunity.createdById);
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", opportunity.accountId);

            return View(opportunity);
        }

        // GET: Opportunities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opportunity opportunity = db.Opportunities.Find(id);
            if (opportunity == null)
            {
                return HttpNotFound();
            }
            ViewBag.opportunityStageId = new SelectList(db.OpportunityStages, "opportunityStageId", "opportunityStageName", opportunity.opportunityStageId);
            ViewBag.assignedToId = new SelectList(db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)), "id", "username", opportunity.assignedToId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", opportunity.createdById);
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", opportunity.accountId);

            var opportunityModel = new OpportunityModel()
            {
                opportunityStageId = opportunity.opportunityStageId,
                assignedToId = opportunity.assignedToId,
                createdById = opportunity.createdById,
                createdDate = opportunity.createdDate,
                opportunityDescription = opportunity.opportunityDescription,
                opportunityEnded = opportunity.opportunityEnded,
                opportunityId = opportunity.opportunityId,
                opportunityName = opportunity.opportunityName,
                tenderCost = (double)opportunity.tenderCost,
                tenderPapersPrice = (double)opportunity.tenderPapersPrice,
                accountId = opportunity.accountId,
                Estimate = db.Estimates.FirstOrDefault(o=>o.tenderId == opportunity.opportunityId)

            };

            return View(opportunityModel);
        }

        // POST: Opportunities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OpportunityModel opportunity)
        {
            if (ModelState.IsValid)
            {
                var opportunityEntity = new Opportunity()
                {
                    opportunityStageId = opportunity.opportunityStageId,
                    assignedToId = opportunity.assignedToId,
                    createdById = opportunity.createdById,
                    createdDate = opportunity.createdDate,
                    opportunityDescription = opportunity.opportunityDescription,
                    opportunityEnded = opportunity.opportunityEnded,
                    opportunityId = opportunity.opportunityId,
                    opportunityName = opportunity.opportunityName,
                    tenderCost = opportunity.tenderCost,
                    tenderPapersPrice = opportunity.tenderPapersPrice,
                    accountId = opportunity.accountId
                };
                db.Entry(opportunityEntity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = opportunityEntity.opportunityId });
            }
            ViewBag.opportunityStageId = new SelectList(db.OpportunityStages, "opportunityStageId", "opportunityStageName", opportunity.opportunityStageId);
            ViewBag.assignedToId = new SelectList(db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)), "id", "username", opportunity.assignedToId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", opportunity.createdById);
            ViewBag.accountId = new SelectList(db.Accounts, "accountId", "accountName", opportunity.accountId);

            return View(opportunity);
        }

        // GET: Opportunities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opportunity opportunity = db.Opportunities.Find(id);
            if (opportunity == null)
            {
                return HttpNotFound();
            }
            return View(opportunity);
        }

        // POST: Opportunities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Opportunity opportunity = db.Opportunities.Find(id);
            db.Opportunities.Remove(opportunity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
