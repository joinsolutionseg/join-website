﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace JoinSolutions.Website.Controllers
{
    public class homeController : BaseController
    {
      
        public ActionResult Dashboard()
        {
            return View();
        }

        //filling language section
        public ActionResult change(string languageAbbrevation)
        {
            Session["language"] = "en";
            if (languageAbbrevation != null)
            {
                Session["languge"] = languageAbbrevation;
            }
            
                
          
            return Redirect(Request.UrlReferrer.ToString());
            //return View(viewName);
        }

        //method to changing Language
        public static void changeLanguage( string session )
        {
            if (session != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(session);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(session);
            }
        }


        // GET: home
        public ActionResult index()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            
            return View();
        }

        ///about
        public ActionResult about()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            return View();
        }


        //contact
        public ActionResult contact()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            return View();
        }

        //contact
        public ActionResult employer()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            return View();
        }

        //coachingSessions
        public ActionResult coachingSessions()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            return View();
        }

        //contact
        public ActionResult jobSeekers()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            return View();
        }

        //contact
        public ActionResult recruiters()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            return View();
        }

        //contact
        public ActionResult coaching()
        {
            if (Session["language"] != null)
            {
                changeLanguage(Session["languge"].ToString());
            }
            return View();
        }


    }
}