﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Enums;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class TasksController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: Tasks
        public ActionResult Index()
        {
            var tasks = db.Tasks.Include(t => t.TaskStatu);
            return View(tasks.ToList());
        }

        // GET: Tasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            ViewBag.taskStatusId = new SelectList(db.TaskStatus, "taskStatusId", "taskStatusName");
            //ViewBag.assignedToId = new SelectList(db.Users, "id", "username",null);
            ViewBag.assignedToId = db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)).Select(x => new SelectListItem() { Text = x.username, Value = x.id.ToString() });
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "taskId,taskName,taskStatusId,isSystem,isActive,taskDescription,assignedToId")] Task task)
        {
            if (ModelState.IsValid)
            {
                task.isActive = true;
                task.isSystem = false;
                task.createdById = ((User)Session["CurrentUser"]).id;
                db.Tasks.Add(task);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.taskStatusId = new SelectList(db.TaskStatus, "taskStatusId", "taskStatusName", task.taskStatusId);
            ViewBag.assignedToId = new SelectList(db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)), "id", "username", task.assignedToId);

            return View(task);
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            ViewBag.taskStatusId = new SelectList(db.TaskStatus, "taskStatusId", "taskStatusName", task.taskStatusId);
            //ViewBag.assignedToId = new SelectList(db.Users, "id", "username", task.assignedToId);
            ViewBag.assignedToId = db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)).Select(x => new SelectListItem() { Text = x.username, Value = x.id.ToString() });

            var taskModel = new TaskModel()
            {
                isActive = task.isActive,
                isSystem = task.isSystem,
                taskId = task.taskId,
                taskName = task.taskName,
                taskStatusId = task.taskStatusId,
                assignedToId = task.assignedToId,
                taskDescription = task.taskDescription,
        };

            return View(taskModel);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "taskId,taskName,taskStatusId,isSystem,isActive,taskDescription,assignedToId")] Task task)
        {
            if (ModelState.IsValid)
            {
                task.createdById = ((User)Session["CurrentUser"]).id;
                db.Entry(task).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.taskStatusId = new SelectList(db.TaskStatus, "taskStatusId", "taskStatusName", task.taskStatusId);
            ViewBag.assignedToId = new SelectList(db.Users.Where(o => o.UserPagePermissions.Any(oo => oo.pageId == (int)Pages.Tasks)), "id", "username", task.assignedToId);

            return View(task);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Task task = db.Tasks.Find(id);
            db.Tasks.Remove(task);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
