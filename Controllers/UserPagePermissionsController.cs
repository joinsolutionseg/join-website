﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Helpers;

namespace JoinSolutions.Website.Controllers
{
    public class UserPagePermissionsController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: UserPagePermissions
        public ActionResult Index()
        {
            var userPagePermissions = db.UserPagePermissions.Include(u => u.Page).Include(u => u.User).Include(u => u.User1).Include(u => u.User2);
            return View(userPagePermissions.ToList());
        }

        // GET: UserPagePermissions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserPagePermission userPagePermission = db.UserPagePermissions.Find(id);
            if (userPagePermission == null)
            {
                return HttpNotFound();
            }
            return View(userPagePermission);
        }

        // GET: UserPagePermissions/Create
        public ActionResult Create()
        {
            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn");
            ViewBag.userId = new SelectList(db.Users, "id", "username");
            ViewBag.createdById = new SelectList(db.Users, "id", "username");
            ViewBag.updateById = new SelectList(db.Users, "id", "username");
            return View();
        }

        // POST: UserPagePermissions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserPagePermission userPagePermission)
        {
            //if (ModelState.IsValid)
            //{
                userPagePermission.createdById = ((User)Session["CurrentUser"]).id;
                userPagePermission.createdDate = LocalDateTime.GetDateTimeNow();
                userPagePermission.updateById = ((User)Session["CurrentUser"]).id;
                userPagePermission.updatedDate = LocalDateTime.GetDateTimeNow();
                userPagePermission.isEnabled = true;
                db.UserPagePermissions.Add(userPagePermission);
                db.SaveChanges();
                return RedirectToAction("Index");
            //}

            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn", userPagePermission.pageId);
            ViewBag.userId = new SelectList(db.Users, "id", "username", userPagePermission.userId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", userPagePermission.createdById);
            ViewBag.updateById = new SelectList(db.Users, "id", "username", userPagePermission.updateById);
            return View(userPagePermission);
        }

        // GET: UserPagePermissions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserPagePermission userPagePermission = db.UserPagePermissions.Find(id);
            if (userPagePermission == null)
            {
                return HttpNotFound();
            }
            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn", userPagePermission.pageId);
            ViewBag.userId = new SelectList(db.Users, "id", "username", userPagePermission.userId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", userPagePermission.createdById);
            ViewBag.updateById = new SelectList(db.Users, "id", "username", userPagePermission.updateById);
            return View(userPagePermission);
        }

        // POST: UserPagePermissions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserPagePermission userPagePermission)
        {
            if (ModelState.IsValid)
            {
                userPagePermission.updateById = ((User)Session["CurrentUser"]).id;
                userPagePermission.updatedDate = LocalDateTime.GetDateTimeNow();
                db.Entry(userPagePermission).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn", userPagePermission.pageId);
            ViewBag.userId = new SelectList(db.Users, "id", "username", userPagePermission.userId);
            ViewBag.createdById = new SelectList(db.Users, "id", "username", userPagePermission.createdById);
            ViewBag.updateById = new SelectList(db.Users, "id", "username", userPagePermission.updateById);
            return View(userPagePermission);
        }

        // GET: UserPagePermissions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserPagePermission userPagePermission = db.UserPagePermissions.Find(id);
            if (userPagePermission == null)
            {
                return HttpNotFound();
            }
            return View(userPagePermission);
        }

        // POST: UserPagePermissions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserPagePermission userPagePermission = db.UserPagePermissions.Find(id);
            db.UserPagePermissions.Remove(userPagePermission);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
