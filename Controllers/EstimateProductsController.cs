﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Filters;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class EstimateProductsController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: EstimateProducts
        public ActionResult Index()
        {
            var estimateProducts = db.EstimateProducts.OrderByDescending(o => o.id).Include(e => e.Product);
            return View(estimateProducts.ToList());
        }

        // GET: EstimateProducts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstimateProduct estimateProduct = db.EstimateProducts.Find(id);
            if (estimateProduct == null)
            {
                return HttpNotFound();
            }
            return View(estimateProduct);
        }

        // GET: EstimateProducts/Create
        public ActionResult Create()
        {
            ViewBag.productId = new SelectList(db.Products, "id", "productSKU");
            return View();
        }

        // POST: EstimateProducts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(EstimateProduct estimateProduct)
        {
            if (ModelState.IsValid)
            {
                db.EstimateProducts.Add(estimateProduct);
                db.SaveChanges();
            }

            ViewBag.productId = new SelectList(db.Products, "id", "productSKU", estimateProduct.productId);
            return RedirectToAction("Edit", "Estimates", new { id = estimateProduct.estimateId });
        }

        // GET: EstimateProducts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstimateProduct estimateProduct = db.EstimateProducts.Find(id);
            if (estimateProduct == null)
            {
                return HttpNotFound();
            }
            ViewBag.productId = new SelectList(db.Products, "id", "productSKU", estimateProduct.productId);
            return View(estimateProduct);
        }

        public ActionResult EditProduct(int? id = 0)
        {
            if (id == 0)
            {
                return PartialView("~/Views/EstimateProducts/_Edit.cshtml", new EstimateProduct() { 
                priceInUSD = 1,
                priceLi = 1,
                profit = 1,
                qty = 1,
                vat = 15,
                periodByMonths = 12
                });
            }
            EstimateProduct estimateProduct = db.EstimateProducts.Find(id);
            if (estimateProduct == null)
            {
                return HttpNotFound();
            }
            estimateProduct.Estimate = db.Estimates.Find(estimateProduct.estimateId);
            estimateProduct.Estimate.EstimateProducts = null;
            ViewBag.productId = new SelectList(db.Products, "id", "productSKU", id);
            return PartialView("~/Views/EstimateProducts/_Edit.cshtml", estimateProduct);
        }

        // POST: EstimateProducts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EstimateProduct estimateProduct)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estimateProduct).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.productId = new SelectList(db.Products, "id", "productSKU", estimateProduct.productId);
            return RedirectToAction("Edit", "Estimates", new { id = estimateProduct.estimateId });
        }

        // GET: EstimateProducts/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    EstimateProduct estimateProduct = db.EstimateProducts.Find(id);
        //    if (estimateProduct == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(estimateProduct);
        //}

        // POST: EstimateProducts/Delete/5
        [ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            EstimateProduct estimateProduct = db.EstimateProducts.Find(id);
            db.EstimateProducts.Remove(estimateProduct);
            db.SaveChanges();
            // return RedirectToAction("Index");
            return RedirectToAction("Edit", "Estimates", new { id = estimateProduct.estimateId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
