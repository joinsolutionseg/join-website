﻿using JoinSolutions.Website.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace JoinSolutions.Website.Controllers
{
    public class BaseController : Controller
    {
        private static string _cookieLangName = "JoinLang";
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // get current culture from request
            var currentCulture = SetCurrentCultureOnThread(filterContext.HttpContext.Request);

            //set en-US dateformat (gregorian calendar) as the default dateformat without pay any attention on which culture in use.
            Thread.CurrentThread.CurrentUICulture.DateTimeFormat = new CultureInfo("en-US").DateTimeFormat;

            // check if session is supported
            //UserClaim objCurrentCustomer = new UserClaim();
            //objCurrentCustomer = ((UserClaim)Session["CurrentUser"]);
            //if (objCurrentCustomer == null)
            //{
            //    // check if a new session id was generated
            //    filterContext.Result = new RedirectResult("~/comingsoon");
            //    return;
            //}


            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // get current culture from request
            var currentCulture = SetCurrentCultureOnThread(filterContext.HttpContext.Request);

            //set en-US dateformat (gregorian calendar) as the default dateformat without pay any attention on which culture in use.
            //if (currentCulture == "ar" || currentCulture == "ar-AE")
            //    Thread.CurrentThread.CurrentUICulture.DateTimeFormat = new CultureInfo("ar-AE").DateTimeFormat;
            //else
                Thread.CurrentThread.CurrentUICulture.DateTimeFormat = new CultureInfo("en-US").DateTimeFormat;

            base.OnActionExecuted(filterContext);
        }

        private static string SetCurrentCultureOnThread(HttpRequestBase request)
        {
            var cookie = request.Cookies[_cookieLangName];
            string cultureOnCookie = string.Empty;
            if (cookie != null)
            {
                cultureOnCookie = cookie.Value;
            }

            string culture = (cultureOnCookie == string.Empty)
                ? CultureHelper.DefaultCulture
                : cultureOnCookie;

            var cultureInfo = new System.Globalization.CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            Thread.CurrentThread.CurrentCulture = cultureInfo;

            return culture;
        }

        public static String GetCultureOnCookie(HttpRequestBase request)
        {
            var cookie = request.Cookies[_cookieLangName];
            string culture = string.Empty;
            if (cookie != null)
            {
                culture = cookie.Value;
            }
            return culture;
        }

    }
}