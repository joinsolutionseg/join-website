﻿using JoinSolutions.Website.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JoinSolutions.Website.Controllers
{
    public class AccountController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: Account
        [Route("LinkedIn")]
        public ActionResult LogIn()
        {
            return View();
        }

        [Route("LinkedIn")]
        [HttpPost]
        public ActionResult LogIn(User user)
        {
            var currentUser = db.Users
                .FirstOrDefault(m => m.username == user.username
                                 && m.password == user.password && m.active == true);

            if (currentUser != null)
            {

                Session["CurrentUser"] = currentUser;

                return RedirectToAction("Index", "Dashboard");
            }
            Session["CurrentUser"] = null;
            ViewBag.error = Resources.users.invalidUser;
            return View();

        }

        public ActionResult LogOut()
        {
            Session["CurrentUser"] = null;

            return RedirectToAction("LogIn");
        }
    }
}