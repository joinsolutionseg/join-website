﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class OpportunityStagesController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: OpportunityStages
        public ActionResult Index()
        {
            return View(db.OpportunityStages.ToList());
        }

        // GET: OpportunityStages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpportunityStage opportunityStage = db.OpportunityStages.Find(id);
            if (opportunityStage == null)
            {
                return HttpNotFound();
            }
            return View(opportunityStage);
        }

        // GET: OpportunityStages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OpportunityStages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "opportunityStageId,opportunityStageName")] OpportunityStage opportunityStage)
        {
            if (ModelState.IsValid)
            {
                db.OpportunityStages.Add(opportunityStage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(opportunityStage);
        }

        // GET: OpportunityStages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpportunityStage opportunityStage = db.OpportunityStages.Find(id);
            if (opportunityStage == null)
            {
                return HttpNotFound();
            }

            var opportuityStageModel = new OpportuityStageModel()
            {
                opportunityStageId = opportunityStage.opportunityStageId,
                opportunityStageName = opportunityStage.opportunityStageName
            };
            return View(opportuityStageModel);
        }

        // POST: OpportunityStages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "opportunityStageId,opportunityStageName")] OpportunityStage opportunityStage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(opportunityStage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(opportunityStage);
        }

        // GET: OpportunityStages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpportunityStage opportunityStage = db.OpportunityStages.Find(id);
            if (opportunityStage == null)
            {
                return HttpNotFound();
            }
            return View(opportunityStage);
        }

        // POST: OpportunityStages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OpportunityStage opportunityStage = db.OpportunityStages.Find(id);
            db.OpportunityStages.Remove(opportunityStage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
