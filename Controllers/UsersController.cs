﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Enums;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Helpers;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class UsersController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: Users
        public ActionResult Index()
        {
            var users = db.Users.OrderByDescending(o => o.id).ToList();
            return View(users);
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        private void FillDropDowns()
        {
            if (CultureHelper.CurrentLanguage == (int)Languages.English)
                ViewBag.roleId = new SelectList(db.Roles, "roleId", "roleNameEn");
            else
                ViewBag.roleId = new SelectList(db.Roles, "roleId", "roleNameAr");
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            FillDropDowns();
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UsersModel user)
        {
            if(db.Users.FirstOrDefault(m=> m.username == user.username) != null)
            {
                ViewBag.userNameError = Resources.users.userNameExists;
                FillDropDowns();
                return View();
            }

            var userEntity = new User()
            {
                userPhone = user.userPhone,
                userEmail = user.userEmail,
                fullName = user.fullName,
                password = user.password,
                username = user.username,
                active = true,
                id = user.id,
                userTitle = user.userTitle,
                userLinkedInProfile = user.userLinkedInProfile,
                roleId = user.roleId
            };
            string uploadPath = "~/Pictures";
            string imageName = SaveNewImage(user.userImageFile, uploadPath, "User", 300, 300);
            userEntity.userImage = imageName;
            db.Users.Add(userEntity);
            db.SaveChanges();

            //insert permissions to new user
            var pages = db.Pages;
            foreach (var page in pages)
            {
                foreach (var permission in page.PagePermissions)
                {
                    var userPermission = new UserPagePermission()
                    {
                        createdById = ((User)Session["CurrentUser"]).id,
                        createdDate = LocalDateTime.GetDateTimeNow(),
                        pageId = page.pageId,
                        permissionId = permission.pagePermissionId,
                        isEnabled = false,
                        userId = userEntity.id,
                        updateById = ((User)Session["CurrentUser"]).id,
                        updatedDate = LocalDateTime.GetDateTimeNow()
                    };

                    db.UserPagePermissions.Add(userPermission);

                }
            }

            db.SaveChanges();

            return RedirectToAction("Index");

        }

        public static string SaveNewImage(HttpPostedFileBase fileToUpload, string savingPath, string imageType, float width, float height)
        {
            if (fileToUpload != null)
            {
                var allowedExtensions = new[] {
                      ".Jpg", ".png", ".jpg", ".jpeg",".JPEG",".GIF"
                    };

                var ext = Path.GetExtension(fileToUpload.FileName);
                if (allowedExtensions.Contains(ext))
                {
                    string name = Path.GetFileNameWithoutExtension(fileToUpload.FileName);
                    string myfile = imageType + LocalDateTime.GetDateTimeNow().ToString("yyyyMMddHHmmssfff") + ext;


                    using (Image image = Image.FromStream(fileToUpload.InputStream, true, false))
                    {
                        var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(savingPath), myfile);
                        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(savingPath)))
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }

                        //Size can be change according to your requirement 
                        float thumbWidth = width;
                        float thumbHeight = height;
                        //calculate  image  size
                        if (image.Width > image.Height)
                        {
                            thumbHeight = ((float)image.Height / image.Width) * thumbWidth;
                        }
                        else
                        {
                            thumbWidth = ((float)image.Width / image.Height) * thumbHeight;
                        }

                        int actualthumbWidth = Convert.ToInt32(Math.Floor(thumbWidth));
                        int actualthumbHeight = Convert.ToInt32(Math.Floor(thumbHeight));
                        var thumbnailBitmap = new Bitmap(actualthumbWidth, actualthumbHeight);
                        var thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
                        thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
                        thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
                        thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        var imageRectangle = new Rectangle(0, 0, actualthumbWidth, actualthumbHeight);
                        thumbnailGraph.DrawImage(image, imageRectangle);
                        var ms = new MemoryStream();
                        thumbnailBitmap.Save(path, ImageFormat.Jpeg);
                        ms.Position = 0;
                        GC.Collect();
                        thumbnailGraph.Dispose();
                        thumbnailBitmap.Dispose();
                        image.Dispose();
                    }

                    return myfile;
                }


            }
            return null;
        }


        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userModel = new UsersModel()
            {
                id = user.id,
                active = user.active,
                password = user.password,
                username = user.username,
                fullName = user.fullName,
                userEmail = user.userEmail,
                userPhone = user.userPhone,
                userTitle = user.userTitle,
                userLinkedInProfile = user.userLinkedInProfile,
                userImage = user.userImage,
                roleId = user.roleId
            };

            FillDropDowns();

            return View(userModel);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UsersModel user)
        {

            var currentUser = db.Users.FirstOrDefault(m => m.id == user.id);

            if(currentUser.username != user.username)
            {
                if (db.Users.FirstOrDefault(m => m.username == user.username) != null)
                {
                    ViewBag.userNameError = Resources.users.userNameExists;
                    FillDropDowns();
                    var userModel = new UsersModel()
                    {
                        id = user.id,
                        active = user.active,
                        password = user.password,
                        username = user.username,
                        fullName = user.fullName,
                        userEmail = user.userEmail,
                        userPhone = user.userPhone,
                        userTitle = user.userTitle,
                        userLinkedInProfile = user.userLinkedInProfile,
                        userImage = user.userImage,
                        roleId = user.roleId
                    };
                    return View(userModel);
                }
            }

            string imagUrl = "";
            if (user.userImageFile != null)
            {
                var pic = System.Web.HttpContext.Current.Request.Files[0];
                imagUrl = SaveNewImage(user.userImageFile, "~/Pictures", "User", 300, 300);

            }
            currentUser.userPhone = user.userPhone;
            currentUser.userEmail = user.userEmail;
            currentUser.fullName = user.fullName;
            currentUser.password = user.password;
            currentUser.username = user.username;
            currentUser.userTitle = user.userTitle;
            currentUser.userLinkedInProfile = user.userLinkedInProfile;
            currentUser.active = user.active;
            currentUser.userImage = user.userImage;
            currentUser.id = user.id;
            currentUser.roleId = user.roleId;
            if(imagUrl != "")
                currentUser.userImage = imagUrl;

            db.SaveChanges();

            

            //insert permissions to new user
            var pages = db.Pages;
            foreach (var page in pages)
            {
                foreach (var permission in page.PagePermissions)
                {
                    var userPermission = db.UserPagePermissions.FirstOrDefault(o => o.userId == currentUser.id && o.permissionId == permission.pageId);
                    if (userPermission == null)
                    {
                        userPermission = new UserPagePermission()
                        {
                            createdById = ((User)Session["CurrentUser"]).id,
                            createdDate = LocalDateTime.GetDateTimeNow(),
                            pageId = page.pageId,
                            permissionId = permission.pagePermissionId,
                            isEnabled = false,
                            userId = currentUser.id,
                            updateById = ((User)Session["CurrentUser"]).id,
                            updatedDate = LocalDateTime.GetDateTimeNow()
                        };

                        db.UserPagePermissions.Add(userPermission);

                    }
                }
            }

            db.SaveChanges();


            return RedirectToAction("Index");

        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);

            var userPermissions = db.UserPagePermissions.Where(o=>o.userId == id);
            foreach (var permission in userPermissions)
            {
                db.UserPagePermissions.Remove(permission);
            }

            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult UserPermissions(int userId)
        {
            return View();
        }

        public JsonResult UpdateUserPermissions(int userId, int permissionId, bool isEnabled)
        {
            var userPermission = db.UserPagePermissions.FirstOrDefault(o => o.userId == userId && o.permissionId == permissionId);
            userPermission.isEnabled = isEnabled;
            db.Entry(userPermission).State = EntityState.Modified;
            var result = db.SaveChanges();

            var currentUserId = ((User)Session["CurrentUser"]).id;
            var currentUser= db.Users.FirstOrDefault(o => o.id == currentUserId); 
            Session["CurrentUser"] = currentUser;

            return Json(result > 0);
        }

    }
}
