﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    public class OfferTypesController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: OfferTypes
        public ActionResult Index()
        {
            return View(db.OfferTypes.ToList());
        }

        // GET: OfferTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfferType offerType = db.OfferTypes.Find(id);
            if (offerType == null)
            {
                return HttpNotFound();
            }
            return View(offerType);
        }

        // GET: OfferTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OfferTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OfferTypeModal offerType)
        {
            if (ModelState.IsValid)
            {
                var offerTypeEntity = new OfferType()
                {
                    offerNameAr = offerType.offerNameAr,
                    offerNameEn = offerType.offerNameEn,
                    offerTypeId = offerType.offerTypeId
                };
                db.OfferTypes.Add(offerTypeEntity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(offerType);
        }

        // GET: OfferTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfferType offerType = db.OfferTypes.Find(id);
            if (offerType == null)
            {
                return HttpNotFound();
            }

            var offerTypeModel = new OfferTypeModal()
            {
                offerNameAr = offerType.offerNameAr,
                offerNameEn = offerType.offerNameEn,
                offerTypeId = offerType.offerTypeId
            };
            return View(offerTypeModel);
        }

        // POST: OfferTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OfferTypeModal offerType)
        {
            if (ModelState.IsValid)
            {
                var offerTypeEntity = new OfferType()
                {
                    offerNameAr = offerType.offerNameAr,
                    offerNameEn = offerType.offerNameEn,
                    offerTypeId = offerType.offerTypeId
                };

                db.Entry(offerTypeEntity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(offerType);
        }

        // GET: OfferTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfferType offerType = db.OfferTypes.Find(id);
            if (offerType == null)
            {
                return HttpNotFound();
            }
            return View(offerType);
        }

        // POST: OfferTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OfferType offerType = db.OfferTypes.Find(id);
            db.OfferTypes.Remove(offerType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
