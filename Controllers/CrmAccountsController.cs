﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;
using JoinSolutions.Website.Filters;
using JoinSolutions.Website.Helpers;
using JoinSolutions.Website.Models;

namespace JoinSolutions.Website.Controllers
{
    [SessionExpireFilter]
    public class CrmAccountsController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: CrmAccounts
        public ActionResult Index()
        {
            return View(db.Accounts.ToList());
        }

        // GET: CrmAccounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // GET: CrmAccounts/Create
        public ActionResult Create()
        {
            ViewBag.accountBillingStatusId = new SelectList(db.AccountBillingStatus, "billingStatusId", "billingStatusName");
            return View();
        }

        // POST: CrmAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Account account)
        {
            if (ModelState.IsValid)
            {
                account.createdDate = LocalDateTime.GetDateTimeNow();
                db.Accounts.Add(account);
                db.SaveChanges();

                var accountTasks = new AccountTask();
                var systemTasks = db.Tasks.Where(m => m.isSystem == true).ToList();
                foreach (var item in systemTasks)
                {
                    accountTasks.accountId = account.accountId;
                    accountTasks.taskId = item.taskId;
                    accountTasks.isCompleted = false;
                    accountTasks.createdById = ((User)Session["CurrentUser"]).id;
                    accountTasks.createdDate = LocalDateTime.GetDateTimeNow();
                    accountTasks.updatedDate = LocalDateTime.GetDateTimeNow();
                    accountTasks.updatedById = ((User)Session["CurrentUser"]).id;

                    db.AccountTasks.Add(accountTasks);
                    db.SaveChanges();
                }


                return RedirectToAction("Create", "Opportunities", new { accountId = account.accountId});
            }

            return View(account);
        }

        // GET: CrmAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }

            ViewBag.accountBillingStatusId = new SelectList(db.AccountBillingStatus, "billingStatusId", "billingStatusName",account.accountBillingStatusId);

            var accountModel = new AccountModel()
            {
                accountBillingStatusId = account.accountBillingStatusId,
                createdDate = account.createdDate,
                accountId = account.accountId,
                accountName = account.accountName,
                accountWebsite = account.accountWebsite,
                accountPhone = account.accountPhone,
                accountEmail = account.accountEmail
            };

            Session["accountId"] = accountModel.accountId;
            Session["accountName"] = account.accountName;

            var opportunities = db.Opportunities.Include(o => o.OpportunityStage).Include(o => o.User)
                .Include(o => o.User1).Where(m=> m.accountId == accountModel.accountId);

            accountModel.opportunity = opportunities.ToList();
            return View(accountModel);
        }

        // POST: CrmAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AccountModel account)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var accountEntity = new Account()
                    {
                        accountEmail = account.accountEmail,
                        accountPhone = account.accountPhone,
                        accountBillingStatusId = account.accountBillingStatusId,
                        accountId = account.accountId,
                        accountName = account.accountName,
                        accountWebsite = account.accountWebsite
                    };
                    accountEntity.createdDate = LocalDateTime.GetDateTimeNow();
                    db.Entry(accountEntity).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception e){
                    var message = e.Message;
                }
                return RedirectToAction("Edit", new { id = account.accountId });
            }
            return View(account);
        }

        // GET: CrmAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // POST: CrmAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Account account = db.Accounts.Find(id);

            var accountTask = db.AccountTasks.Where(m => m.accountId == account.accountId).ToList();
            foreach (var item in accountTask)
            {
                db.AccountTasks.Remove(item);
                db.SaveChanges();
            }

            db.Accounts.Remove(account);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
