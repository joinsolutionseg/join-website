﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JoinSolutions.Website.Data;

namespace JoinSolutions.Website.Controllers
{
    public class PagePermissionsController : BaseController
    {
        private JoinDBEntities db = new JoinDBEntities();

        // GET: PagePermissions
        public ActionResult Index()
        {
            var pagePermissions = db.PagePermissions.Include(p => p.Page);
            return View(pagePermissions.ToList());
        }

        // GET: PagePermissions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PagePermission pagePermission = db.PagePermissions.Find(id);
            if (pagePermission == null)
            {
                return HttpNotFound();
            }
            return View(pagePermission);
        }

        // GET: PagePermissions/Create
        public ActionResult Create()
        {
            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn");
            return View();
        }

        // POST: PagePermissions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PagePermission pagePermission)
        {
            if (ModelState.IsValid)
            {
                db.PagePermissions.Add(pagePermission);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn", pagePermission.pageId);
            return View(pagePermission);
        }

        // GET: PagePermissions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PagePermission pagePermission = db.PagePermissions.Find(id);
            if (pagePermission == null)
            {
                return HttpNotFound();
            }
            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn", pagePermission.pageId);
            return View(pagePermission);
        }

        // POST: PagePermissions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PagePermission pagePermission)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pagePermission).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pageId = new SelectList(db.Pages, "pageId", "pageNameEn", pagePermission.pageId);
            return View(pagePermission);
        }

        // GET: PagePermissions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PagePermission pagePermission = db.PagePermissions.Find(id);
            if (pagePermission == null)
            {
                return HttpNotFound();
            }
            return View(pagePermission);
        }

        // POST: PagePermissions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PagePermission pagePermission = db.PagePermissions.Find(id);
            db.PagePermissions.Remove(pagePermission);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
