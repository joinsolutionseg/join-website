﻿using JoinSolutions.Website.Resources;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Models
{
    public class SuppliersModel
    {
        public int id { get; set; }
        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "supplierNameRequired")]
        [Display(Name = "supplierName", ResourceType = typeof(estimates))]
        public string name { get; set; }
    }
}