﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Models
{
    public class OpportuityStageModel
    {
        public int opportunityStageId { get; set; }
        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "opportunityStageRequired")]
        [Display(Name = "opportunityStage", ResourceType = typeof(estimates))]
        public string opportunityStageName { get; set; }
    }
}