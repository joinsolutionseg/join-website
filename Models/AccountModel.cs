﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using Resources;
using JoinSolutions.Website.Resources;
using JoinSolutions.Website.Data;

namespace JoinSolutions.Website.Models
{
    public class AccountModel
    {
        public AccountModel()
        {

        }
       
        public int accountId { get; set; }

        [Required(ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "accountNameRequired")]
        [Display(Name = "accountName", ResourceType = typeof(task))]
        public string accountName { get; set; }

        [Display(Name = "accountBillingStatus", ResourceType = typeof(task))]
        public int accountBillingStatusId { get; set; }

        [Required(ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "accountWebsiteRequired")]
        [Display(Name = "accountWebsite", ResourceType = typeof(task))]
        [Url]
        public string accountWebsite { get; set; }

        public string accountPhone { get; set; }
        public string accountEmail { get; set; }

        public System.DateTime createdDate { get; set; }

        public List<Opportunity> opportunity { get; set; }

    }

}