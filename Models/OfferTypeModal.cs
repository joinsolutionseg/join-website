﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Models
{
    public class OfferTypeModal
    {
        public int offerTypeId { get; set; }

        [Required(ErrorMessageResourceType = typeof(estimates), ErrorMessageResourceName = "offerNameEnRequiredMessage")]
        public string offerNameEn { get; set; }

        [Required(ErrorMessageResourceType = typeof(estimates), ErrorMessageResourceName = "offerNameArRequiredMessage")]
        public string offerNameAr { get; set; }
    }
}