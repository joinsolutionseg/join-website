﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Models
{
    public class UsersModel
    {
        public int id { get; set; }
        [Required(ErrorMessageResourceType = typeof(users),
             ErrorMessageResourceName = "usernameRequired")]
        [Display(Name = "username", ResourceType = typeof(users))]
        public string username { get; set; }

        [Required(ErrorMessageResourceType = typeof(users),
             ErrorMessageResourceName = "passwordRequired")]
        [Display(Name = "password", ResourceType = typeof(users))]
        public string password { get; set; }

        [Display(Name = "active", ResourceType = typeof(users))]
        public bool active { get; set; }

        [Display(Name = "fullName", ResourceType = typeof(estimates))]
        public string fullName { get; set; }

        [Display(Name = "userPhone", ResourceType = typeof(estimates))]
        public string userPhone { get; set; }

        [Display(Name = "userEmail", ResourceType = typeof(estimates))]
        public string userEmail { get; set; }

        public string userImage { get; set; }
        [Display(Name = "userTitle", ResourceType = typeof(estimates))]
        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "userTitleRequired")]
        public string userTitle { get; set; }
        [Display(Name = "userLinkedInProfile", ResourceType = typeof(estimates))]
        [Url]
        //[Required(ErrorMessageResourceType = typeof(estimates),
        //     ErrorMessageResourceName = "userLinkedRequired")]
        public string userLinkedInProfile { get; set; }

        [Display(Name = "userRole", ResourceType = typeof(estimates))]
        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "userRoleRequired")]
        public Nullable<int> roleId { get; set; }

        [Display(Name = "userImage", ResourceType = typeof(estimates))]
        //[Required(ErrorMessageResourceType = typeof(estimates),
        //    ErrorMessageResourceName = "Please_select_file")]
        [RegularExpression(@"^.*\.(jpg|gif|jpeg|png|bmp|JPG|GIF|JPEG|PNG|BMP)$", ErrorMessageResourceType = typeof(estimates),
            ErrorMessageResourceName = "Only_Image_files_allowed")]
        public HttpPostedFileBase userImageFile { get; set; }
    }
}