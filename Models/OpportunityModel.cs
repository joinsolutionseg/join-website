﻿using JoinSolutions.Website.Data;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Models
{
    public class OpportunityModel
    {
        public int opportunityId { get; set; }

        [Required(ErrorMessageResourceType = typeof(opportunities),ErrorMessageResourceName = "opportunityNameRequired")]
        public string opportunityName { get; set; }

        [Required(ErrorMessageResourceType = typeof(opportunities),ErrorMessageResourceName = "opportunityStageRequired")]
        public int opportunityStageId { get; set; }

        [Required(ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "tenderPapersPriceRequired")]
        [Display(Name = "tenderPapersPrice", ResourceType = typeof(task))]
        public double tenderPapersPrice { get; set; }

        [Required(ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "tenderCostRequired")]
        [Display(Name = "tenderCost", ResourceType = typeof(task))]
        public double tenderCost { get; set; }

        [Required(ErrorMessageResourceType = typeof(opportunities),ErrorMessageResourceName = "assignedToRequired")]
        public int assignedToId { get; set; }
        public int createdById { get; set; }
        public DateTime createdDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(opportunities),ErrorMessageResourceName = "opportunityEndedRequired")]
        public DateTime opportunityEnded { get; set; }
        [StringLength(500, ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "maximumLengh")]
        public string opportunityDescription { get; set; }
        public int? accountId { get; set; }
        public Estimate Estimate { get; set; }

    }
}