﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using Resources;
using JoinSolutions.Website.Resources;

namespace JoinSolutions.Website.Models
{
    public class ProductModel
    {
        public ProductModel()
        {
           // this.EstimateProducts E:\Test\join-website\Views\Products\Create.cshtml= new HashSet<EstimateProduct>();
        }

        public int id { get; set; }
        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "productSKURequired")]
        [Display(Name = "productSKU", ResourceType = typeof(estimates))]
        public string productSKU { get; set; }

        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "productNameEnRequired")]
        [Display(Name = "productNameEn", ResourceType = typeof(estimates))]
        public string productNameEn { get; set; }

        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "productNameArRequired")]
        [Display(Name = "productNameAr", ResourceType = typeof(estimates))]
        public string productNameAr { get; set; }

        [Required(ErrorMessageResourceType = typeof(estimates),
             ErrorMessageResourceName = "productNameArRequired")]
        [Display(Name = "catrgoryName", ResourceType = typeof(estimates))]
        public int productCategoryId { get; set; }

        [Display(Name = "supplierName", ResourceType = typeof(estimates))]
        public Nullable<int> supplierId { get; set; }
        
        [Display(Name = "productDescriptionEn", ResourceType = typeof(estimates))]
        [StringLength(500, ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "maximumLengh")]
        public string productDescriptionEn { get; set; }

        [Display(Name = "productDescriptionAr", ResourceType = typeof(estimates))]
        [StringLength(500, ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "maximumLengh")]
        public string productDescriptionAr { get; set; }
    }

}