﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using Resources;
using JoinSolutions.Website.Resources;

namespace JoinSolutions.Website.Models
{
    public class TaskModel
    {
        public TaskModel()
        {

        }
        public int taskId { get; set; }

        [Required(ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "taskNameRequired")]
        [Display(Name = "taskName", ResourceType = typeof(task))]
        public string taskName { get; set; }

        [Display(Name = "taskStatusName", ResourceType = typeof(task))]
        public int taskStatusId { get; set; }

        [Display(Name = "isSystem", ResourceType = typeof(task))]
        public bool isSystem { get; set; }

        [Display(Name = "isActive", ResourceType = typeof(task))]
        public bool isActive { get; set; }

        [Display(Name = "assignedTo", ResourceType = typeof(task))]
        public Nullable<int> assignedToId { get; set; }
        

        [Display(Name = "taskDescription", ResourceType = typeof(task))]
        [Required(ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "taskDescriptionRquired")]
        [StringLength(500,ErrorMessageResourceType = typeof(task),
             ErrorMessageResourceName = "maximumLengh")]
        public string taskDescription { get; set; }


    }

}