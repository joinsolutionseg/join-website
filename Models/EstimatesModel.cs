﻿using JoinSolutions.Website.Data;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Models
{
    public class EstimatesModel
    {
        public EstimatesModel()
        {
            EstimateProducts = new List<EstimateProduct>();
        }

        public int id { get; set; }
        public int? tenderId { get; set; }
        [Required(ErrorMessageResourceType = typeof(estimates),ErrorMessageResourceName = "customerNameRequired")]
        [Display(Name = "customerName", ResourceType = typeof(estimates))]
        public int? accountId { get; set; }

        [Required(ErrorMessageResourceType = typeof(estimates), ErrorMessageResourceName = "proposalTypeRequired")]
        public int? offerTypeId { get; set; }

        public string description { get; set; }

        
        public string customerName { get; set; }

        [Required(ErrorMessageResourceType = typeof(estimates),ErrorMessageResourceName = "estimateDateRequired")]
        public System.DateTime estimateDate { get; set; }
        public decimal currencyRate { get; set; }
        public int statusId { get; set; }
        public string customerNotes { get; set; }
        public string estimateTerms { get; set; }
        public string customerPhone { get; set; }
        public string customerEmail { get; set; }
        public Nullable<int> periodByMonths { get; set; }
        public virtual ICollection<EstimateProduct> EstimateProducts { get; set; }


    }
}