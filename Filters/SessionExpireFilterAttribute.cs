﻿using JoinSolutions.Website.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JoinSolutions.Website.Filters
{
    public class SessionExpireFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // check if session is supported
            User objCurrentCustomer = new User();
            objCurrentCustomer = ((User)ctx.Session["CurrentUser"]);

            if (objCurrentCustomer == null)
            {
                // check if a new session id was generated
                filterContext.Result = new RedirectResult("/Account/login");
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}