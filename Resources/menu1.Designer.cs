﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class menu {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal menu() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("JoinSolutions.Website.Resources.menu", typeof(menu).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ABOUT US.
        /// </summary>
        public static string about {
            get {
                return ResourceManager.GetString("about", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to COACHING.
        /// </summary>
        public static string coaching {
            get {
                return ResourceManager.GetString("coaching", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CONTACT US.
        /// </summary>
        public static string contact {
            get {
                return ResourceManager.GetString("contact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EMPLOYER.
        /// </summary>
        public static string employer {
            get {
                return ResourceManager.GetString("employer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to JOB SEEKERS.
        /// </summary>
        public static string jobseekers {
            get {
                return ResourceManager.GetString("jobseekers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RECRUITERS.
        /// </summary>
        public static string recruiters {
            get {
                return ResourceManager.GetString("recruiters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search here....
        /// </summary>
        public static string search {
            get {
                return ResourceManager.GetString("search", resourceCulture);
            }
        }
    }
}
