﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class employer {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal employer() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("JoinSolutions.Website.Resources.employer", typeof(employer).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Why We&apos;re Here.
        /// </summary>
        public static string emp1 {
            get {
                return ResourceManager.GetString("emp1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Every company is unique and every candidate wants something different from their work. We believe that employers and candidates are ultimately looking for the same thing.
        /// </summary>
        public static string emp1des {
            get {
                return ResourceManager.GetString("emp1des", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to What We Do.
        /// </summary>
        public static string emp2 {
            get {
                return ResourceManager.GetString("emp2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We forge more authentic connections between employers and candidates by providing an inside look at your company’s culture, workplace, and values..
        /// </summary>
        public static string emp2des {
            get {
                return ResourceManager.GetString("emp2des", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Who We Are.
        /// </summary>
        public static string emp3 {
            get {
                return ResourceManager.GetString("emp3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Our audience of next-gen talent is diverse and engaged. Millions of qualified candidates come to The Muse every year in search of the next great place to grow their careers—and your company should be visible when they’re ready to make their next move..
        /// </summary>
        public static string emp3des {
            get {
                return ResourceManager.GetString("emp3des", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to YOU NEED TO HIRE TOPTALENT.
        /// </summary>
        public static string heading {
            get {
                return ResourceManager.GetString("heading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HOW WE DO WHAT WE DO.
        /// </summary>
        public static string section2head {
            get {
                return ResourceManager.GetString("section2head", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to STAND OUT TO THE TALENT YOU WANT TO ATTRACT!.
        /// </summary>
        public static string subheading {
            get {
                return ResourceManager.GetString("subheading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BRANDING.
        /// </summary>
        public static string we1 {
            get {
                return ResourceManager.GetString("we1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We use those insights to produce and collect photos and videos, then build a company profile that gives an authentic look..
        /// </summary>
        public static string we1des {
            get {
                return ResourceManager.GetString("we1des", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to STORYTELLING.
        /// </summary>
        public static string we2 {
            get {
                return ResourceManager.GetString("we2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Once we’ve captured your company story, we help you share it everywhere—from your careers page to your social media..
        /// </summary>
        public static string we2des {
            get {
                return ResourceManager.GetString("we2des", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RECRUITNG.
        /// </summary>
        public static string we3 {
            get {
                return ResourceManager.GetString("we3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We use proprietary technology and our expertise to gather insights directly from your employees to learn what they value the most..
        /// </summary>
        public static string we3des {
            get {
                return ResourceManager.GetString("we3des", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CONSULTING.
        /// </summary>
        public static string we4 {
            get {
                return ResourceManager.GetString("we4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to With your unique story consistently being showcased in front of top talent, you will attract, hire, and retain the right people for your team..
        /// </summary>
        public static string we4des {
            get {
                return ResourceManager.GetString("we4des", resourceCulture);
            }
        }
    }
}
