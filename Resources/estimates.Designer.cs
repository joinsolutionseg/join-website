﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class estimates {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal estimates() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("JoinSolutions.Website.Resources.estimates", typeof(estimates).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to action.
        /// </summary>
        public static string action {
            get {
                return ResourceManager.GetString("action", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Actions.
        /// </summary>
        public static string Actions {
            get {
                return ResourceManager.GetString("Actions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add/ Edit Product.
        /// </summary>
        public static string addEditProduct {
            get {
                return ResourceManager.GetString("addEditProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Product.
        /// </summary>
        public static string AddNewProduct {
            get {
                return ResourceManager.GetString("AddNewProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Product.
        /// </summary>
        public static string AddProduct {
            get {
                return ResourceManager.GetString("AddProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Product Category.
        /// </summary>
        public static string AddProductCategory {
            get {
                return ResourceManager.GetString("AddProductCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Proposal Type.
        /// </summary>
        public static string AddProposalType {
            get {
                return ResourceManager.GetString("AddProposalType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Supplier.
        /// </summary>
        public static string AddSupplier {
            get {
                return ResourceManager.GetString("AddSupplier", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add User.
        /// </summary>
        public static string AddUser {
            get {
                return ResourceManager.GetString("AddUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Proposals.
        /// </summary>
        public static string AllEstimates {
            get {
                return ResourceManager.GetString("AllEstimates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back to Proposals.
        /// </summary>
        public static string BackToEstimates {
            get {
                return ResourceManager.GetString("BackToEstimates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back to Tender Stages.
        /// </summary>
        public static string backToOpportunityStages {
            get {
                return ResourceManager.GetString("backToOpportunityStages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back To Product Catgories.
        /// </summary>
        public static string BackToProductCatgories {
            get {
                return ResourceManager.GetString("BackToProductCatgories", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back ToProducts.
        /// </summary>
        public static string BackToProducts {
            get {
                return ResourceManager.GetString("BackToProducts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back To Proposal Types.
        /// </summary>
        public static string BackToProposalTypes {
            get {
                return ResourceManager.GetString("BackToProposalTypes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back To Suppliers.
        /// </summary>
        public static string BackToSuppliers {
            get {
                return ResourceManager.GetString("BackToSuppliers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BackToTenders.
        /// </summary>
        public static string BackToTenders {
            get {
                return ResourceManager.GetString("BackToTenders", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back To Users.
        /// </summary>
        public static string BackToUsers {
            get {
                return ResourceManager.GetString("BackToUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Catrgory name.
        /// </summary>
        public static string catrgoryName {
            get {
                return ResourceManager.GetString("catrgoryName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Catrgory name required.
        /// </summary>
        public static string catrgoryNameRequired {
            get {
                return ResourceManager.GetString("catrgoryNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conditions:.
        /// </summary>
        public static string Conditions_ {
            get {
                return ResourceManager.GetString("Conditions:", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cost Price.
        /// </summary>
        public static string CostPrice {
            get {
                return ResourceManager.GetString("CostPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create.
        /// </summary>
        public static string Create {
            get {
                return ResourceManager.GetString("Create", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Proposal.
        /// </summary>
        public static string CreateNew {
            get {
                return ResourceManager.GetString("CreateNew", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create new Tender Stage.
        /// </summary>
        public static string createNewOpportunityStage {
            get {
                return ResourceManager.GetString("createNewOpportunityStage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Currency rate.
        /// </summary>
        public static string currencyRate {
            get {
                return ResourceManager.GetString("currencyRate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Currency rate required.
        /// </summary>
        public static string currencyRateRequired {
            get {
                return ResourceManager.GetString("currencyRateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account Email.
        /// </summary>
        public static string customerEmail {
            get {
                return ResourceManager.GetString("customerEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account Name.
        /// </summary>
        public static string customerName {
            get {
                return ResourceManager.GetString("customerName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account Name is required.
        /// </summary>
        public static string customerNameRequired {
            get {
                return ResourceManager.GetString("customerNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Notes.
        /// </summary>
        public static string customerNotes {
            get {
                return ResourceManager.GetString("customerNotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account Phone.
        /// </summary>
        public static string customerPhone {
            get {
                return ResourceManager.GetString("customerPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string delete1 {
            get {
                return ResourceManager.GetString("delete1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete Proposal Type.
        /// </summary>
        public static string DeleteProposalType {
            get {
                return ResourceManager.GetString("DeleteProposalType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PorposalsName.
        /// </summary>
        public static string description {
            get {
                return ResourceManager.GetString("description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Details.
        /// </summary>
        public static string details {
            get {
                return ResourceManager.GetString("details", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string edit1 {
            get {
                return ResourceManager.GetString("edit1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Proposal.
        /// </summary>
        public static string EditEstimate {
            get {
                return ResourceManager.GetString("EditEstimate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Product.
        /// </summary>
        public static string EditProduct {
            get {
                return ResourceManager.GetString("EditProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Product Category.
        /// </summary>
        public static string EditProductCategory {
            get {
                return ResourceManager.GetString("EditProductCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Proposal Type.
        /// </summary>
        public static string EditProposalType {
            get {
                return ResourceManager.GetString("EditProposalType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Supplier.
        /// </summary>
        public static string EditSupplier {
            get {
                return ResourceManager.GetString("EditSupplier", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit User.
        /// </summary>
        public static string EditUser {
            get {
                return ResourceManager.GetString("EditUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your username and password to access admin panel..
        /// </summary>
        public static string Enter_your_username_and_password {
            get {
                return ResourceManager.GetString("Enter your username and password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Date.
        /// </summary>
        public static string estimateDate {
            get {
                return ResourceManager.GetString("estimateDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal date required.
        /// </summary>
        public static string estimateDateRequired {
            get {
                return ResourceManager.GetString("estimateDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Estimate Name.
        /// </summary>
        public static string EstimateName {
            get {
                return ResourceManager.GetString("EstimateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposals.
        /// </summary>
        public static string Estimates {
            get {
                return ResourceManager.GetString("Estimates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal  Terms.
        /// </summary>
        public static string estimateTerms {
            get {
                return ResourceManager.GetString("estimateTerms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Full Name.
        /// </summary>
        public static string fullName {
            get {
                return ResourceManager.GetString("fullName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Index.
        /// </summary>
        public static string index {
            get {
                return ResourceManager.GetString("index", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LogIn.
        /// </summary>
        public static string LogIn {
            get {
                return ResourceManager.GetString("LogIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Proposal.
        /// </summary>
        public static string New_Estimate {
            get {
                return ResourceManager.GetString("New Estimate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Type Name (Arabic).
        /// </summary>
        public static string offerNameAr {
            get {
                return ResourceManager.GetString("offerNameAr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Type Name (Arabic)  is required.
        /// </summary>
        public static string offerNameArRequiredMessage {
            get {
                return ResourceManager.GetString("offerNameArRequiredMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Type Name (English).
        /// </summary>
        public static string offerNameEn {
            get {
                return ResourceManager.GetString("offerNameEn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Type Name (English) is required.
        /// </summary>
        public static string offerNameEnRequiredMessage {
            get {
                return ResourceManager.GetString("offerNameEnRequiredMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Type.
        /// </summary>
        public static string OfferType {
            get {
                return ResourceManager.GetString("OfferType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal Types.
        /// </summary>
        public static string OfferTypes {
            get {
                return ResourceManager.GetString("OfferTypes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Offer Validity: 30 days.
        /// </summary>
        public static string OfferValidity_30_Days {
            get {
                return ResourceManager.GetString("OfferValidity_30_Days", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Only Image files allowed.
        /// </summary>
        public static string Only_Image_files_allowed {
            get {
                return ResourceManager.GetString("Only_Image_files_allowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Competition stage.
        /// </summary>
        public static string opportunityStage {
            get {
                return ResourceManager.GetString("opportunityStage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tender stage required.
        /// </summary>
        public static string opportunityStageRequired {
            get {
                return ResourceManager.GetString("opportunityStageRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tender Stages.
        /// </summary>
        public static string opportunityStages {
            get {
                return ResourceManager.GetString("opportunityStages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment: After supply and activation of the services.
        /// </summary>
        public static string Payment_After_the_supply_of_the_receipt {
            get {
                return ResourceManager.GetString("Payment_After_the_supply_of_the_receipt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Period By Months.
        /// </summary>
        public static string periodByMonths {
            get {
                return ResourceManager.GetString("periodByMonths", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permissions.
        /// </summary>
        public static string Permissions {
            get {
                return ResourceManager.GetString("Permissions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select user file.
        /// </summary>
        public static string Please_select_file {
            get {
                return ResourceManager.GetString("Please_select_file", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prices are in Saudi Riyal.
        /// </summary>
        public static string Price__are_in_saudi_riyals_ {
            get {
                return ResourceManager.GetString("Price _are_in_saudi_riyals.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price(SAR).
        /// </summary>
        public static string PriceInSAR {
            get {
                return ResourceManager.GetString("PriceInSAR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price(USD).
        /// </summary>
        public static string priceInUSD {
            get {
                return ResourceManager.GetString("priceInUSD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print.
        /// </summary>
        public static string Print {
            get {
                return ResourceManager.GetString("Print", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Categories.
        /// </summary>
        public static string ProductCategories {
            get {
                return ResourceManager.GetString("ProductCategories", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Category is required.
        /// </summary>
        public static string ProductCategoryRequired {
            get {
                return ResourceManager.GetString("ProductCategoryRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Description in Arabic.
        /// </summary>
        public static string productDescriptionAr {
            get {
                return ResourceManager.GetString("productDescriptionAr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Description in English.
        /// </summary>
        public static string productDescriptionEn {
            get {
                return ResourceManager.GetString("productDescriptionEn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        public static string productName {
            get {
                return ResourceManager.GetString("productName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Name in Arabic.
        /// </summary>
        public static string productNameAr {
            get {
                return ResourceManager.GetString("productNameAr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Name in Arabic Required.
        /// </summary>
        public static string productNameArRequired {
            get {
                return ResourceManager.GetString("productNameArRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Name.
        /// </summary>
        public static string productNameEn {
            get {
                return ResourceManager.GetString("productNameEn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Name in English.
        /// </summary>
        public static string productNameEn1 {
            get {
                return ResourceManager.GetString("productNameEn1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Name in English Required.
        /// </summary>
        public static string productNameEnRequired {
            get {
                return ResourceManager.GetString("productNameEnRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to products.
        /// </summary>
        public static string products {
            get {
                return ResourceManager.GetString("products", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product SKU.
        /// </summary>
        public static string productSKU {
            get {
                return ResourceManager.GetString("productSKU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product SKU Required.
        /// </summary>
        public static string productSKURequired {
            get {
                return ResourceManager.GetString("productSKURequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profit.
        /// </summary>
        public static string profit {
            get {
                return ResourceManager.GetString("profit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User.
        /// </summary>
        public static string ProposalCreator {
            get {
                return ResourceManager.GetString("ProposalCreator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal type is required.
        /// </summary>
        public static string proposalTypeRequired {
            get {
                return ResourceManager.GetString("proposalTypeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Qty.
        /// </summary>
        public static string qty {
            get {
                return ResourceManager.GetString("qty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SAR.
        /// </summary>
        public static string SAR {
            get {
                return ResourceManager.GetString("SAR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show.
        /// </summary>
        public static string Show {
            get {
                return ResourceManager.GetString("Show", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proposal.
        /// </summary>
        public static string ShowEstimate {
            get {
                return ResourceManager.GetString("ShowEstimate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string status {
            get {
                return ResourceManager.GetString("status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status required.
        /// </summary>
        public static string statusRequired {
            get {
                return ResourceManager.GetString("statusRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cost Price Total.
        /// </summary>
        public static string Sub_total {
            get {
                return ResourceManager.GetString("Sub_total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Supplier name.
        /// </summary>
        public static string supplierName {
            get {
                return ResourceManager.GetString("supplierName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Supplier name required.
        /// </summary>
        public static string supplierNameRequired {
            get {
                return ResourceManager.GetString("supplierNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Suppliers.
        /// </summary>
        public static string Suppliers {
            get {
                return ResourceManager.GetString("Suppliers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activation: One week after receiving the PO or the signing of the contract.
        /// </summary>
        public static string Supply_A_week_after_the_baptism_or_the_signing_of_the_contract {
            get {
                return ResourceManager.GetString("Supply_A_week_after_the_baptism_or_the_signing_of_the_contract", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vat.
        /// </summary>
        public static string Tax {
            get {
                return ResourceManager.GetString("Tax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total.
        /// </summary>
        public static string Total {
            get {
                return ResourceManager.GetString("Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total Cost.
        /// </summary>
        public static string TotalCost {
            get {
                return ResourceManager.GetString("TotalCost", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total(SAR).
        /// </summary>
        public static string TotalInSAR {
            get {
                return ResourceManager.GetString("TotalInSAR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total(USD).
        /// </summary>
        public static string TotalInUSD {
            get {
                return ResourceManager.GetString("TotalInUSD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upload.
        /// </summary>
        public static string Upload {
            get {
                return ResourceManager.GetString("Upload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string userEmail {
            get {
                return ResourceManager.GetString("userEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User image.
        /// </summary>
        public static string userImage {
            get {
                return ResourceManager.GetString("userImage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User linked In profile.
        /// </summary>
        public static string userLinkedInProfile {
            get {
                return ResourceManager.GetString("userLinkedInProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User linked In required.
        /// </summary>
        public static string userLinkedRequired {
            get {
                return ResourceManager.GetString("userLinkedRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Permissions.
        /// </summary>
        public static string UserPermissions {
            get {
                return ResourceManager.GetString("UserPermissions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone.
        /// </summary>
        public static string userPhone {
            get {
                return ResourceManager.GetString("userPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User role.
        /// </summary>
        public static string userRole {
            get {
                return ResourceManager.GetString("userRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User role required.
        /// </summary>
        public static string userRoleRequired {
            get {
                return ResourceManager.GetString("userRoleRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Users.
        /// </summary>
        public static string Users {
            get {
                return ResourceManager.GetString("Users", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User title.
        /// </summary>
        public static string userTitle {
            get {
                return ResourceManager.GetString("userTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to user Title required.
        /// </summary>
        public static string userTitleRequired {
            get {
                return ResourceManager.GetString("userTitleRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vat .
        /// </summary>
        public static string vat {
            get {
                return ResourceManager.GetString("vat", resourceCulture);
            }
        }
    }
}
