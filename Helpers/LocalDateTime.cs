﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace JoinSolutions.Website.Helpers
{
    public static class LocalDateTime
    {
        public static string areaTimeZone = "Egypt Standard Time";

        public static DateTime GetDateTimeNow()
        {
            //Get Time Zone Info by Id to convert Current Time to it
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(areaTimeZone);

            //Get Current Date Time and Convert it to selected time zone
            //to get current time to server time
            DateTime dateTime = DateTime.Now.AddHours(-1);
            return TimeZoneInfo.ConvertTime(dateTime, timeZone);
        }

        public static DateTime GetDateToday()
        {
            //Get Time Zone Info by Id to convert Current Time to it
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(areaTimeZone);

            //Get Current Date Time and Convert it to selected time zone
            //to get current time to server time
            DateTime dateTime = DateTime.Today;
            return TimeZoneInfo.ConvertTime(dateTime, timeZone);
        }

        public static string SetDateFormat(DateTime date, string format = "MM/dd/yyyy")
        {
            return date.ToString(format, CultureInfo.InvariantCulture);
        }
    }
}