﻿using JoinSolutions.Website.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Configuration;

namespace JoinSolutions.Website.Helpers
{
    public static class CultureHelper
    {
        public static string CurrentCulture
        {
            get
            {
                string cookie = null;
                if (HttpContext.Current.Request.Cookies["JoinLang"] != null)
                    cookie = HttpContext.Current.Request.Cookies["JoinLang"].Value;
                return cookie ?? Thread.CurrentThread.CurrentUICulture.Name;
            }
        }

        public static int CurrentLanguage
        {
            get
            {
                var culture = string.IsNullOrEmpty(CultureHelper.CurrentCulture) ? CultureHelper.DefaultCulture : CultureHelper.CurrentCulture;
                return (culture == "en-US" || culture == "en") ? Convert.ToInt32(Languages.English) : Convert.ToInt32(Languages.Arabic);
            }
        }

        public static string DefaultCulture
        {
            get
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("/");
                GlobalizationSection section = (GlobalizationSection)config.GetSection("system.web/globalization");
                return section.UICulture;
            }
        }
    }

}